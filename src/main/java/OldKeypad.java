
import java.util.HashMap;
import java.util.Scanner;

public class OldKeypad {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        HashMap<Character, String> incode = incode();
        String input=scanner.nextLine();
        String output="";
//        System.out.println(incode.get('i'));
        for (int i=0;i<input.length();i++){
            output+=incode.get(input.charAt(i));
        }
        System.out.println(output);
    }

    static HashMap incode() {

        HashMap<Character, String> incode = new HashMap<>();
        incode.put('.', "1");
        incode.put('!', "11");
        incode.put('?', "111");
        incode.put('a', "2");
        incode.put('b', "22");
        incode.put('c', "222");
        incode.put('d', "3");
        incode.put('e', "33");
        incode.put('f', "333");
        incode.put('g', "4");
        incode.put('h', "44");
        incode.put('i', "444");
        incode.put('j', "5");
        incode.put('k', "55");
        incode.put('l', "555");
        incode.put('m', "6");
        incode.put('n', "66");
        incode.put('o', "666");
        incode.put('p', "7");
        incode.put('q', "77");
        incode.put('r', "777");
        incode.put('s', "8");
        incode.put('t', "88");
        incode.put('u', "888");
        incode.put('v', "9");
        incode.put('w', "99");
        incode.put('x', "999");
        incode.put('y', "0");
        incode.put('z', "00");
        incode.put(' ', "000");
        return incode;
    }
}