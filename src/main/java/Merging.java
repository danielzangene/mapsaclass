import java.util.ArrayList;
import java.util.Arrays;

public class Merging {
    public static void main(String[] args) {
        int[] a = new int[]{-1, 2, 10, 50};
        int[] b = new int[]{5, 20, 30, 70, 80, 90, 100};
        System.out.println(pr(merge(a, b)));
    }

    static String pr(int[] a) {
        String str = "";
        for (int i = 0; i < a.length; i++) {
            str += a[i] + ",";
        }
        return str;
    }

    static int[] merge(int[] a, int[] b) {
        int[] c = new int[a.length + b.length];
        int i = 0, j = 0, k = 0;
        while (i < a.length && j < b.length) {
            if (a[i] < b[j]) {
                c[k] = a[i];
                i++;
                k++;
            } else {
                c[k] = b[j];
                k++;
                j++;
            }
        }
        if (i < a.length) {
            for (; i < a.length; i++, k++) {
                c[k] = a[i];
            }
        } else {
            for (; j < b.length; j++, k++)
                c[k] = b[j];
        }
        return c;
    }
}
