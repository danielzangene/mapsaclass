import java.lang.reflect.Array;
import java.util.*;

public class POP {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str=scanner.nextLine();
        if (str.length() % 2 != 0)
            str += "1";
        while(str.length()>1) {

            String[] input = str.split("");
            str = "";
            for (int i = 0; i < input.length; i += 2) {
                int a = Integer.parseInt(input[i]);
                int b = Integer.parseInt(input[i + 1]);
                String s = String.valueOf(a * b);
                str += s;
            }

            System.out.println(str);
        }
    }
}